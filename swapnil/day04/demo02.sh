#!/bin/bash

arr=(1 2 3 4 5)

echo "Array of element :${arr[*]}"

echo "No of elements :${#arr[*]}"

res=0;
for var in ${arr[*]}
do
  res=`expr $var + $res`
done
echo "result :$res"

str="Sunbeam"

echo -e "\n$str"
